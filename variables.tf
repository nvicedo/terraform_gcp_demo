variable "project" { }

variable "credentials_file" { }

variable "region" {
  default = "europe-west1"
}

variable "zone" {
  default = "europe-west1-b"
}

variable "cidrs" {
  default = []
}

variable "environment" {
  type    = string
  default = "dev"
}

variable "machine_types" {
  type    = map
  default = {
    dev  = "f1-micro"
    test = "e2-micro"
    prod = "e2-medium"
  }
}
