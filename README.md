# Terraform GCP demo

Terraform lab base on [Terraform official GCP tutorial](https://learn.hashicorp.com/collections/terraform/gcp-get-started).

## Usage

- Create a `terraform.tfvars` file

- Edit with the following values:

```terraform
project = "<PROJECT_NAME>"
credentials_file = "<CREDENTIALS_JSON_FILE_LOCATION>"
region = "<REGION>" # by default "europe-west1"
zone = "<ZONE>" # by default "europe-west1-b"
cidrs = [ "<CIDR>", "<CIDR2>" ] # At least 1 CIDR
environment = "<ENVIRONMENT>" # can be either "dev" (default), "test", "prod"
```

- Apply Terraform

```bash
terraform apply -auto-approve
```
